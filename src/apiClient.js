import contentful from "contentful";

console.log(import.meta.env.CONTENTFUL_API_ACCESS_TOKEN);

export default contentful.createClient({
  space: "hmkzfswlr1a4",
  accessToken: import.meta.env.CONTENTFUL_API_ACCESS_TOKEN
});
